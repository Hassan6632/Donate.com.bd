<?php 
include ('header.php');
include ('config.php');

$sql=$db->query('SELECT * FROM slider');
$books = $sql->fetchAll(PDO::FETCH_ASSOC);

?>

<!--[Slider Start]-->
<section id="slide">
    <div class="slide-carouse owl-carousel">
        <?php foreach ($books as $book): ?>
        <div class="slide-content slide-content-bg-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 m-md-auto">
                        <div class="slide-table">
                            <div class="slide-table-cell">
                                <div class="slide-txt">
                                    <h1><span><?php echo $book['slide_title'] ?>  </span>
                                        <?php echo $book['slide_title_black'] ?> </h1>
                                    <p>
                                        <?php echo $book['slide_Content'] ?> </p>
                                    <div class="slide-btn">
                                        <a href="" class="boxed-btn">
                                            <?php echo $book['slide_btn'] ?>
                                        </a>
                                        <a href="" class="boxed-btn">Check Shedule</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</section>
<!--/. [ End Slider ]-->

<!--    [ Strat Section Title Area]-->
<section id="project-link">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3">
                <a href="#" class="proj-link-btn"><i class="icofont icofont-unity-hand"></i> <span>Project 01</span></a>
            </div>
            <div class="col-lg-3">
                <a href="#" class="proj-link-btn"><i class="icofont icofont-unity-hand"></i> <span>Project 02</span></a>
            </div>
            <div class="col-lg-3">
                <a href="#" class="proj-link-btn"><i class="icofont icofont-unity-hand"></i> <span>Project 03</span></a>
            </div>
            <div class="col-lg-3">
                <a href="#" class="proj-link-btn"><i class="icofont icofont-unity-hand"></i> <span>Project 04</span></a>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<section id="gallery" data-top="background-position:0px 0px" data-top-bottom="background-position:0px -100px">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Project Image</h2>
                    </div>
                </div>
            </div>
            <div class="gallery-content text-center">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery gallery-bg-1 d-table">
                            <div class="gallery-content d-table-cell">
                                <h3>Project Title</h3>
                                <p>Lorem ipsum dolor sit amet.</p>
                                <a href="#" class="pro-detail btn">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery gallery-bg-2 d-table">
                            <div class="gallery-content d-table-cell">
                                <h3>Project Title</h3>
                                <p>Lorem ipsum dolor sit amet.</p>
                                <a href="#" class="pro-detail btn">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery gallery-bg-3 d-table">
                            <div class="gallery-content d-table-cell">
                                <h3>Project Title</h3>
                                <p>Lorem ipsum dolor sit amet.</p>
                                <a href="#" class="pro-detail btn">Detail</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="gallery gallery-bg-4 d-table">
                            <div class="gallery-content d-table-cell">
                                <h3>Project Title</h3>
                                <p>Lorem ipsum dolor sit amet.</p>
                                <a href="#" class="pro-detail btn">Detail</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="gallery gallery-bg-5 d-table">
                            <div class="gallery-content d-table-cell">
                                <h3>Project Title</h3>
                                <p>Lorem ipsum dolor sit amet.</p>
                                <a href="#" class="pro-detail btn">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<section id="sister-consern" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-8 m-md-auto text-center">
                <div class="section-title bg-dark-title">
                    <h2>Our Partners</h2>
                </div>
            </div>
        </div>
        <div class="partner-logo">
            <div class="slide-partner-logo owl-carousel">
                <div class="partner-logo-pic text-center"><img src="assets/img/prelab.png" alt=""></div>
                <div class="partner-logo-pic text-center"><img src="assets/img/prelab.png" alt=""></div>
                <div class="partner-logo-pic text-center"><img src="assets/img/prelab.png" alt=""></div>
                <div class="partner-logo-pic text-center"><img src="assets/img/prelab.png" alt=""></div>
                <div class="partner-logo-pic text-center"><img src="assets/img/prelab.png" alt=""></div>
                <div class="partner-logo-pic text-center"><img src="assets/img/prelab.png" alt=""></div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->


<?php include ('footer.php');?>
