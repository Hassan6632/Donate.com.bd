-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2018 at 07:03 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `donate_com_bd`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaign_title`
--

CREATE TABLE IF NOT EXISTS `campaign_title` (
  `id` int(11) NOT NULL,
  `pro_title` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campaign_title`
--

INSERT INTO `campaign_title` (`id`, `pro_title`) VALUES
(1, 'Project Title 01');

-- --------------------------------------------------------

--
-- Table structure for table `donar_goods`
--

CREATE TABLE IF NOT EXISTS `donar_goods` (
  `id` int(11) NOT NULL,
  `product` varchar(100) NOT NULL,
  `organization` varchar(150) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `address` text NOT NULL,
  `area` varchar(150) NOT NULL,
  `number` int(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donar_goods`
--

INSERT INTO `donar_goods` (`id`, `product`, `organization`, `date`, `time`, `address`, `area`, `number`) VALUES
(1, 'opel', 'PreneurLAB', '2018-02-14', '23:11:00', 'Do you know function are also object. if you know can u give me a simple example.\r\nif you don''t know also comment.', 'Zikatola', 0),
(2, 'opel', 'saab', '2018-02-14', '10:11:00', 'zikatola, Dhaka', 'volvo', 1825573355);

-- --------------------------------------------------------

--
-- Table structure for table `sec_pro_link`
--

CREATE TABLE IF NOT EXISTS `sec_pro_link` (
  `id` int(11) NOT NULL,
  `pro_name` varchar(200) NOT NULL,
  `pro_link` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sec_pro_link`
--

INSERT INTO `sec_pro_link` (`id`, `pro_name`, `pro_link`) VALUES
(1, 'Preneur Lab', 'https://www.facebook.com/peace.digital/'),
(2, 'Preneur Lab', 'https://www.facebook.com/peace.digital/'),
(3, 'Preneur Lab', 'https://www.facebook.com/peace.digital/'),
(4, 'Preneur Lab 02', 'https://www.facebook.com/0002'),
(5, 'Preneur Lab 02', 'https://www.facebook.com/0002');

-- --------------------------------------------------------

--
-- Table structure for table `short_pro_home`
--

CREATE TABLE IF NOT EXISTS `short_pro_home` (
  `id` int(11) NOT NULL,
  `pro_title` varchar(300) NOT NULL,
  `pro_desc` text NOT NULL,
  `pro_link` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `short_pro_home`
--

INSERT INTO `short_pro_home` (`id`, `pro_title`, `pro_desc`, `pro_link`) VALUES
(1, 'Project Title', 'Lorem ipsum dolor sit amet.', 'https://www.facebook.com/peace.digital/'),
(2, 'Project Title 03', 'Lorem ipsum dolor sit amet. 03', 'https://www.facebook.com/0002');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL,
  `slide_title` varchar(300) NOT NULL,
  `slide_title_black` varchar(300) NOT NULL,
  `slide_Content` text NOT NULL,
  `slide_btn` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slide_title`, `slide_title_black`, `slide_Content`, `slide_btn`) VALUES
(1, 'Get the tech that', 'gets people talking', 'With both live crowdfunding campaigns and innovative products shipping now, thereâ€™s no better place to start the hunt for cool and clever innovations that surprise and delight. Indiegogo is where new launches.', 'TEST'),
(2, 'Get the tech that', 'gets people talking', 'With both live crowdfunding campaigns and innovative products shipping now, thereâ€™s no better place to start the hunt for cool and clever innovations that surprise and delight. Indiegogo is where new launches.', 'TEST'),
(3, 'Get the tech that', 'gets people talking', 'With both live crowdfunding campaigns and innovative products shipping now, thereâ€™s no better place to start the hunt for cool and clever innovations that surprise and delight. Indiegogo is where new launches.', 'TEST'),
(4, 'Get the tech that', 'gets people talking', 'With both live crowdfunding campaigns and innovative products shipping now, thereâ€™s no better place to start the hunt for cool and clever innovations that surprise and delight. Indiegogo is where new launches.', 'TEST'),
(5, 'Get the tech that', 'gets people talking', 'With both live crowdfunding campaigns and innovative products shipping now, thereâ€™s no better place to start the hunt for cool and clever innovations that surprise and delight. Indiegogo is where new launches.', 'TEST'),
(6, 'Be a part of emerging tech,', 'design, and much more', 'New to crowdfunding? Welcome! With Indiegogo''s crowdfunding and InDemand campaigns, there''s no better place to discover ingenious products just as they emerge from the minds of creative entrepreneurs around the world.', 'EXPLORE CAMPAIGN'),
(7, 'Get the tech that', 'design, and much more', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam eveniet, nesciunt, quam blanditiis reiciendis commodi asperiores sunt nobis quibusdam quas, nam natus. Voluptate nemo laudantium, sequi. Maxime nihil odio, nisi.', 'TEST 02');

-- --------------------------------------------------------

--
-- Table structure for table `tk_donate`
--

CREATE TABLE IF NOT EXISTS `tk_donate` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `amount` int(20) NOT NULL,
  `number` int(20) NOT NULL,
  `transaction` varchar(50) NOT NULL,
  `area` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_donate`
--

INSERT INTO `tk_donate` (`id`, `name`, `amount`, `number`, `transaction`, `area`) VALUES
(1, 'rashed', 0, 1825573355, '57B675dd', 'Zikatola'),
(2, 'Hassan', 2000, 1913366632, '90BS55641', 'Jatrabari');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campaign_title`
--
ALTER TABLE `campaign_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donar_goods`
--
ALTER TABLE `donar_goods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sec_pro_link`
--
ALTER TABLE `sec_pro_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `short_pro_home`
--
ALTER TABLE `short_pro_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_donate`
--
ALTER TABLE `tk_donate`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campaign_title`
--
ALTER TABLE `campaign_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `donar_goods`
--
ALTER TABLE `donar_goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sec_pro_link`
--
ALTER TABLE `sec_pro_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `short_pro_home`
--
ALTER TABLE `short_pro_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tk_donate`
--
ALTER TABLE `tk_donate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
