<?php 
include 'header.php';

if(isset($_POST['submit'])){
    
    $slide_title=$_POST['slide_title'];
    $slide_title_black=$_POST['slide_title_black'];
    $slide_Content=$_POST['slide_Content'];
    $slide_btn=$_POST['slide_btn'];
    
    include ('config.php');
    
    $stmt=$db->prepare('INSERT INTO slider(slide_title, slide_title_black, slide_Content, slide_btn)VALUES(:slide_title, :slide_title_black, :slide_Content, :slide_btn)');
    
    $stmt->bindParam('slide_title', $slide_title);
    $stmt->bindParam('slide_title_black', $slide_title_black);
    $stmt->bindParam('slide_Content', $slide_Content);
    $stmt->bindParam('slide_btn', $slide_btn);
    
    $stmt->execute();
    
    if($stmt->rowCount()){
        echo 'sucess';
    }
}
    

?>

<style>
    .cpanel-slide-info span {
        display: block;
        margin-bottom: 5px;
    }

    .cpanel-slide-info input {
        width: 100%;
        height: 40px;
        padding: 0 10px;
    }

    .cpanel-slide-info {
        margin: 15px 0;
    }

</style>
<section id="slider">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Home Slider</h2>
                    </div>
                </div>
            </div>
            <form action="" method="post">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="cpanel-slide-info">
                            <span>Slide Heading</span>
                            <input type="text" name="slide_title" placeholder="Slide Heading Green">
                        </div>
                        <div class="cpanel-slide-info">
                            <span>Slide Heading</span>
                            <input type="text" name="slide_title_black" placeholder="Slide Heading Black">
                        </div>
                        <div class="cpanel-slide-info">
                            <span>Slide Detail Content</span>
                            <input type="text" name="slide_Content" placeholder="Slide Content">
                        </div>
                        <div class="cpanel-slide-info">
                            <span>Slide Button Name</span>
                            <input type="text" name="slide_btn" placeholder="Slide Content">
                        </div>
                        <div class="cpanel-slide-info">
                            <span>Slide Button Name</span>
                            <input type="submit" name="submit" value="submit">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<?php include 'footer.php'?>
