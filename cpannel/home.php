<?php 

    include('header.php'); 

    include ('../config.php');

if(isset($_POST['submit1'])){
    
    $slide_title=$_POST['slide_title'];
    $slide_title_black=$_POST['slide_title_black'];
    $slide_Content=$_POST['slide_Content'];
    $slide_btn=$_POST['slide_btn'];
    
    include ('config.php');
    
    $stmt=$db->prepare('INSERT INTO slider(slide_title, slide_title_black, slide_Content, slide_btn)VALUES(:slide_title, :slide_title_black, :slide_Content, :slide_btn)');
    
    $stmt->bindParam('slide_title', $slide_title);
    $stmt->bindParam('slide_title_black', $slide_title_black);
    $stmt->bindParam('slide_Content', $slide_Content);
    $stmt->bindParam('slide_btn', $slide_btn);
    
    $stmt->execute();
    
    if($stmt->rowCount()){
        echo 'sucess';
    }
}

    if(isset($_POST['submit2'])){
        $pro_name=$_POST['pro_name'];
        $pro_link=$_POST['pro_link'];
        
      
        
        $stmt=$db->prepare('INSERT INTO sec_pro_link(pro_name, pro_link)VALUES(:pro_name, :pro_link)');
        
        $stmt->bindParam('pro_name', $pro_name);
        $stmt->bindParam('pro_link', $pro_link);
        
        $stmt->execute();
        
        if($stmt->rowCount()){
            echo 'sucess';
        }
        
        
    }

    if(isset($_POST['submit3'])){
        $pro_title=$_POST['pro_title'];
        $pro_desc=$_POST['pro_desc'];
        $pro_link=$_POST['pro_link'];        
            
        $stmt=$db->prepare('INSERT INTO short_pro_home(pro_title, pro_desc, pro_link)VALUES(:pro_title, :pro_desc, :pro_link)');
        
        $stmt->bindParam('pro_title', $pro_title);
        $stmt->bindParam('pro_desc', $pro_desc);
        $stmt->bindParam('pro_link', $pro_link);
        
        $stmt->execute();
        
        if($stmt->rowCount()){
            echo 'Sucess';
        }
    }



?>
<section id="panel-home">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Admin Panel Home</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="home-menu">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Slider Option</a></li>
                            <li><a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">New Link</a></li>
                            <li><a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Project Title</a></li>
                            <li><a class="nav-link" id="partners-tab" data-toggle="tab" href="#partners" role="tab" aria-controls="contact" aria-selected="false">Add Partners</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form action="" method="post">
                                <div class="row justify-content-center">
                                    <div class="col-lg-12">
                                        <div class="cpanel-slide-info">
                                            <span>Slide Heading</span>
                                            <input type="text" name="slide_title" placeholder="Slide Heading Green">
                                        </div>
                                        <div class="cpanel-slide-info">
                                            <span>Slide Heading</span>
                                            <input type="text" name="slide_title_black" placeholder="Slide Heading Black">
                                        </div>
                                        <div class="cpanel-slide-info">
                                            <span>Slide Detail Content</span>
                                            <textarea name="slide_Content" placeholder="Slide Content"></textarea>
                                            <!--<input type="text" name="slide_Content" placeholder="Slide Content">-->
                                        </div>
                                        <div class="cpanel-slide-info">
                                            <span>Slide Button Name</span>
                                            <input type="text" name="slide_btn" placeholder="Slide Content">
                                        </div>
                                        <div class="cpanel-slide-info">
                                            <span>Slide Button Name</span>
                                            <input type="submit" name="submit1" value="submit">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <form action="" method="post">
                                <div class="home-pro-link">
                                    <span>Project Name</span>
                                    <input type="text" name="pro_name" placeholder="Project Name">
                                    <span>Project Link</span>
                                    <input type="link" name="pro_link" placeholder="Project Link">
                                    <input type="submit" value="Submit" name="submit2">
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <form action="" method="post">
                                <div class="home-pro-link">
                                    <span>Project Title</span>
                                    <input type="text" name="pro_title" placeholder="Project Title">
                                    <span>Short Dercription</span>
                                    <input type="text" name="pro_desc" placeholder="Short Dercription">
                                    <span>Detail Link</span>
                                    <input type="link" name="pro_link" placeholder="Detail Link">
                                    <span>Upload Background Image</span>
                                    <input type="file" name="pro_img">
                                    <input type="submit" value="submit" name="submit3">
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="partners" role="tabpanel" aria-labelledby="partners-tab">
                            <form action="" method="post">
                                <div class="home-pro-link">
                                    <span>Upload Partner Image</span>
                                    <input type="file" name="pro_img">
                                    <input type="submit" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>
