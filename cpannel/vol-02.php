<?php include('header.php'); ?>
<section id="volunteer-accept">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Volunteer Information</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="volunteer-alart">
                        <div class="vol-alrt-content">
                            <img src="assets/img/03.jpg" alt="">
                            <div class="donate-info">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="alert-vol-info">
                                            <h4><span>Preneur LAB</span></h4>
                                            <h4><span>Pick Up Date: </span> 2018-02-14</h4>
                                            <h4><span>Pick Up Time : </span> 23:11:00 </h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="alert-vol-info">
                                            <h4><span>Address : </span>21/A Jigatala, Dhanmondi, Dhaka-1212, Bangladesh</h4>
                                            <h4><span>Contact : </span> +8801670343183</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="volunteer-accept">
                            <input type="submit" name="accept" value="Accept">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('footer.php'); ?>
