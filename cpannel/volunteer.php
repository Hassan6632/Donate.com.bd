<?php 

include ('header.php');
include ('../config.php');

$sql = $db->query('SELECT * FROM donar_goods');
$books = $sql->fetchAll(PDO::FETCH_ASSOC);

?>

<!--    [ Strat Section Area]-->
<section id="volunteer">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Cpanel</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <?php foreach ($books as $book): ?>
                    <div class="vol-analytics">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="vol-pic">
                                    <img src="assets/img/03.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="vol-info">
                                    <h4><span class="cm-identity">Preneur Lab</span></h4>
                                    <h4> <span>PickUp Dae :</span>
                                        <?php echo $book['date']; ?>
                                    </h4>
                                    <h4><span>PickUp Time:</span>
                                        <?php echo $book['time']; ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="vol-info">
                                    <h4><span>Address :</span>
                                        <?php echo $book['address']; ?>
                                    </h4>
                                    <h4><span>Collector Contact :</span>
                                        <?php echo $book['number']; ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="volunteer-accept">
                                    <input type="submit" name="accept" value="Accept">
                                    <input type="submit" name="Decline" value="Decline">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<?php include ('footer.php'); ?>
