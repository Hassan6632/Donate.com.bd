<?php include('header.php'); ?>

<section id="register">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Cpanel</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <a href="home.php" class="reg-btn text-center">
                       <span class="reg-content">
                           <i class="icofont icofont-help-robot"></i>
                            <!--<i class="icofont icofont-hand-drag1"></i>-->
                            <h4>Home Edit</h4>
                       </span>                    
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="add-project.php" class="reg-btn text-center">
                       <span class="reg-content">
                           <i class="icofont icofont-ui-rate-add"></i>
                            <h4>Add Project</h4>
                       </span>                    
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="volunteer.php" class="reg-btn text-center">
                       <span class="reg-content">
                           <i class="icofont icofont-notification"></i>
                            <h4>Volunteer Alert</h4>
                       </span>                    
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="vol-02.php" class="reg-btn text-center">
                       <span class="reg-content">
                           <i class="icofont icofont-notification"></i>
                            <h4>Volunteer Alert 02</h4>
                       </span>                    
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>
