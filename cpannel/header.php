<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>Donate.com.BD</title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">

    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="assets/css/magnific-popup.css" />


    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">


</head>

<body>

    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat Logo Area]-->
        <div class="main-menu">
            <nav class="navbar navbar-expand-lg fixed-top nav-bg-col">
                <div class="container">
                    <a href="index.php" class="navbar-brand"><span class="logo"><img src="assets/img/logo.png" alt=""></span></a>
                    <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbar9">
                        <span class="navbar-toggler-icon"><i class="icofont icofont-justify-all"></i></span>
                    </button>
                    <div class="navbar-collapse collapse" id="navbar9">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="home.php">Home Edit</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="add-project.php">Add Project</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="vol-02.php">Volunteer</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->
