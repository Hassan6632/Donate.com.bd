<?php 

include('header.php'); 
include ('../config.php');

if(isset($_POST['submit'])){
    $pro_title=$_POST['pro_title'];
    
    $stmt=$db->prepare('INSERT INTO campaign_title(pro_title)VALUE(:pro_title)');
    
    $stmt->bindParam('pro_title', $pro_title);
    
    $stmt->execute();
    
    if($stmt->rowCount()){
        echo 'sucess';
    }
    
}

if(isset($_POST['submit2'])){
    $disc_heading=$_POST['disc_heading'];
    $pro_disc=$_POST['pro_disc'];
    $pro_video_link=$_POST['pro_video_link'];
    
    $stmt=$db->prepare('INSERT INTO project_disription(disc_heading, pro_disc, pro_video_link)VALUES(:disc_heading, :pro_disc, :pro_video_link)');
    
    $stmt->bindparam('disc_heading', $disc_heading);
    $stmt->bindparam('pro_disc', $pro_disc);
    $stmt->bindparam('pro_video_link', $pro_video_link);
    
    $stmt->execute();
    
    if($stmt->rowCount()){
        echo 'Sucess';
    }
}

if(isset($_POST['submit3'])){
    $pro_amount=$_POST['pro_amount'];
    
    $stmt=$db->prepare('INSERT INTO target_amount(pro_amount)VALUE(:pro_amount)');
    
    $stmt->bindParam('pro_amount', $pro_amount);
    
    $stmt->execute();
    
    if($stmt->rowCount()){
        echo 'Sucess';
    }
}

if(isset($_POST['submit4'])){
    $strt_date=$_POST['strt_date'];
    $end_date=$_POST['end_date'];
    
    $stmt=$db->prepare('INSERT INTO campaign_time(strt_date, end_date)VALUES(:strt_date, :end_date)');
    
    $stmt->bindParam('strt_date', $strt_date);
    $stmt->bindParam('end_date', $end_date);
    
    $stmt->execute();
    
    if($stmt->rowCount()){
        echo 'Sucess';
    }
}
?>
<section id="panel-home">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Admin Panel Home</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="home-menu">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Campaigner Logo</a></li>
                            <li><a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Campaign Title</a></li>
                            <li><a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Project Descrption</a></li>
                            <li><a class="nav-link" id="partners-tab" data-toggle="tab" href="#partners" role="tab" aria-controls="contact" aria-selected="false">Campaign donate</a></li>
                            <li><a class="nav-link" id="campaign-time" data-toggle="tab" href="#camp-time" role="tab" aria-controls="contact" aria-selected="false">Campaign time</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form action="" method="post">
                                <div class="home-pro-link">
                                    <span>Upload Company Logo</span>
                                    <input type="file" name="pro_img">
                                    <input type="submit" value="Submit">
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <form action="" method="post">
                                <div class="home-pro-link">
                                    <span>Project Title</span>
                                    <input type="text" name="pro_title" placeholder="Project Title">
                                    <span>Upload Background Image</span>
                                    <input type="file" name="pro_img">
                                    <input type="submit" value="Submit" name="submit">
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <form action="" method="post">
                                <div class="home-pro-link">
                                    <span>Description Heading</span>
                                    <input type="text" name="disc_heading" placeholder="Description Heading">
                                    <span>Project Description</span>
                                    <textarea name="pro_disc" id="" placeholder="Project Description"></textarea>
                                    <span>Video Link</span>
                                    <input type="link" name="pro_video_link" placeholder="Video Link">
                                    <span>Upload Project/Video Image</span>
                                    <input type="file" name="pro_img">
                                    <input type="submit" value="Submit" name="submit2">
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="partners" role="tabpanel" aria-labelledby="partners-tab">
                            <form action="" method="post">
                                <div class="home-pro-link">
                                    <span>Target Amount</span>
                                    <input type="number" name="pro_amount" placeholder="Target Amount">

                                    <input type="submit" value="Submit" name="submit3">
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="camp-time" role="tabpanel" aria-labelledby="campaign-time">
                            <form action="" method="post">
                                <div class="home-pro-link">
                                    <span>Campaign Start Date</span>
                                    <input type="date" name="strt_date" placeholder="Campaign Start Date">
                                    <span>Campaign End Date</span>
                                    <input type="date" name="end_date" placeholder="Campaign End Date">
                                    <input type="submit" value="Submit" name="submit4">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('footer.php'); ?>
