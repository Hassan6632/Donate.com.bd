<?php include('header.php'); ?>

<section id="doner-reg">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Donar Registration</h2>
                    </div>
                </div>
            </div>

            <form action="" method="post">
                <div class="reg-form">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="reg-info">
                                <span>Name Company</span>
                                <input type="text" name="name" placeholder="Name Company">
                            </div>
                            <div class="reg-info">
                                <span>Email</span>
                                <input type="email" name="email" placeholder="Email">
                            </div>
                            <div class="reg-info">
                                <span>Phone</span>
                                <input type="number" name="email" placeholder="Phone">
                            </div>
                            <div class="reg-info">
                                <span>Address</span>
                                <textarea name="" id="" placeholder="Address"></textarea>
                            </div>
                            <div class="reg-info">
                                <span>Organization Type</span>
                                <!--<input type="text" name="organization_type" placeholder="Organization Type">-->
                                <input list="thana" name="organization_type" placeholder="Organization Type">
                                <datalist id="thana">
                                            <option value="Community-based Organizations"></option>
                                            <option value="Citywide Organizations"></option>
                                            <option value="National NGOs"></option>
                                            <option value="International NGOs"></option>
                                            <option value="Empowering Orientation "></option>
                                            <option value="Participatory Orientation"></option>
                                </datalist>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="reg-info">
                                <div class="pro-pic">
                                    <img id="test" src="#" alt="" />
                                    <i class="icofont icofont-user-alt-3"></i>
                                </div>
                                <div class="up-pic-btn file-input-wrapper">
                                    <button class="btn-file-input"><i class="icofont icofont-image"></i> Photo / Logo</button>
                                    <input id="tstpic" type="file" name="photo" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="reg-info">
                                        <span>Organization Registered</span>
                                        <div class="ngo-registered">
                                            <p>Yes/No</p>
                                            <input id="show" type="radio" name="email" value="Yes">
                                            <input id="hide" type="radio" name="email" value="No">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="reg-info reg-number">
                                        <span>Socity Registration Number</span>
                                        <input type="text" name="reg_number" placeholder="Socity Registration Number">
                                    </div>
                                </div>
                            </div>
                            <div class="row minus-margin">
                                <div class="col-lg-6">
                                    <div class="reg-info">
                                        <span>District</span>
                                        <input type="text" name="email" placeholder="District">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="reg-info">
                                        <span>Police Station</span>
                                        <input list="thana" name="thana" placeholder="Police Station">
                                        <datalist id="thana">
                                            <option value="Shibganj"></option>
                                            <option value="Haripur"></option>
                                            <option value="Baliadangi"></option>
                                            <option value="Chapai Nawabganj Sadar"></option>
                                            <option value="Ranisankail"></option>
                                            <option value="Bholahat"></option>
                                            <option value="Gomastapur"></option>
											<option value="Nachole"></option>
											<option value="Godagari"></option>
											<option value="Thakurgaon Sadar"></option>
											<option value="Tentulia"></option>
											<option value="Pirganj"></option>
											<option value="Atwari"></option>
											<option value="Bochaganj"></option>
											<option value="Niamatpur"></option>
											<option value="Porsha"></option>
											<option value="Panchagarh Sadar"></option>
											<option value="Sapahar"></option>
											<option value="Tanore"></option>
											<option value="Biral"></option>
											<option value="Paba"></option>
											<option value="Boda"></option>
											<option value="Kaharole"></option>
											<option value="Rajpara"></option>
											<option value="Birganj"></option>
											<option value="Meherpur Sadar"></option>
											<option value="Boalia"></option>
											<option value="Mujib Nagar"></option>
											<option value="Shah Makhdum"></option>
											<option value="Dinajpur Sadar"></option>
											<option value="Mohanpur"></option>
											<option value="Patnitala"></option>
											<option value="Matihar"></option>
											<option value="Debiganj"></option>
											<option value="Gangni"></option>
											<option value="Manda"></option>
											<option value="Damurhuda"></option>
											<option value="Durgapur"></option>
											<option value="Dhamoirhat"></option>
											<option value="Charghat"></option>
											<option value="Mahadebpur"></option>
											<option value="Baghmara"></option>
											<option value="Puthia"></option>
											<option value="Khansama"></option>
											<option value="Chirirbandar"></option>
											<option value="Bagha"></option>
											<option value="Maheshpur"></option>
											<option value="Daulatpur"></option>
											<option value="Nilphamari Sadar"></option>
											<option value="Domar"></option>
											<option value="Naogaon Sadar"></option>
											<option value="Chuadanga Sadar"></option>
											<option value="Alamdanga"></option>
											<option value="Jiban Nagar"></option>
											<option value="Parbatipur"></option>
											<option value="Birampur"></option>
											<option value="Bagati Para"></option>
											<option value="Lalpur"></option>
											<option value="Saidpur"></option>
											<option value="Badalgachhi"></option>
											<option value="Sharsha"></option>
											<option value="Atrai"></option>
											<option value="Fulbari"></option>
											<option value="Jaldhaka"></option>
											<option value="Chaugachha"></option>
											<option value="Dimla"></option>
											<option value="Natore Sadar"></option>
											<option value="Mirpur"></option>
											<option value="Raninagar"></option>
											<option value="Patgram"></option>
											<option value="Kalaroa"></option>
											<option value="Satkhira Sadar"></option>
											<option value="Bheramara"></option>
											<option value="Taraganj"></option>
											<option value="Kotchandpur"></option>
											<option value="Debhata"></option>
											<option value="Joypurhat Sadar"></option>
											<option value="Badarganj"></option>
											<option value="Kishoreganj"></option>
											<option value="Adamdighi"></option>
											<option value="Kushtia Sadar"></option>
											<option value="Jhenaidah Sadar"></option>
											<option value="Akkelpur"></option>
											<option value="Hakimpur"></option>
											<option value="Panchbibi"></option>
											<option value="Kaliganj"></option>
											<option value="Ishwardi"></option>
											<option value="Harinakunda"></option>
											<option value="Jhikargachha"></option>
											<option value="Shyamnagar"></option>
											<option value="Nawabganj"></option>
											<option value="Singra"></option>
											<option value="Kaliganj"></option>
											<option value="Baraigram"></option>
											<option value="Khetlal"></option>
											<option value="Rangpur Sadar"></option>
											<option value="Hatibandha"></option>
											<option value="Dhupchanchia"></option>
											<option value="Tala"></option>
											<option value="Keshabpur"></option>
											<option value="Mitha Pukur"></option>
											<option value="Kotwali"></option>
											<option value="Manirampur"></option>
											<option value="Gangachara"></option>
											<option value="Pabna Sadar"></option>
											<option value="Assasuni"></option>
											<option value="Kumarkhali"></option>
											<option value="Ghoraghat"></option>
											<option value="Shailkupa"></option>
											<option value="Pirganj"></option>
											<option value="Kalai"></option>
											<option value="Gurudaspur"></option>
											<option value="Nandigram"></option>
											<option value="Atgharia"></option>
											<option value="Kaliganj"></option>
											<option value="Kahaloo"></option>
											<option value="Gobindaganj"></option>
											<option value="Shibganj"></option>
											<option value="Chatmohar"></option>
											<option value="Bagher Para"></option>
											<option value="Paikgachha"></option>
											<option value="Khoksa"></option>
											<option value="Palashbari"></option>
											<option value="Shalikha"></option>
											<option value="Koyra"></option>
											<option value="Bogra Sadar"></option>
											<option value="Tarash"></option>
											<option value="Dumuria"></option>
											<option value="Aditmari"></option>
											<option value="Magura Sadar"></option>
											<option value="Shajahanpur"></option>
											<option value="Pangsha"></option>
											<option value="Pirgachha"></option>
											<option value="Kaunia"></option>
											<option value="Abhaynagar"></option>
											<option value="Sadullapur"></option>
											<option value="Sherpur"></option>
											<option value="Bhangura"></option>
											<option value="Sreepur"></option>
											<option value="Narail Sadar"></option>
											<option value="Lalmonirhat Sadar"></option>
											<option value="Gabtali"></option>
											<option value="Phultala"></option>
											<option value="Sujanagar"></option>
											<option value="Santhia"></option>
											<option value="Kalukhali"></option>
											<option value="Batiaghata"></option>
											<option value="Sundarganj"></option>
											<option value="Faridpur"></option>
											<option value="Ullah Para"></option>
											<option value="Royganj"></option>
											<option value="Rajarhat"></option>
											<option value="Sonatola"></option>
											<option value="Balia Kandi"></option>
											<option value="Gaibandha Sadar"></option>
											<option value="Dacope"></option>
											<option value="Khan Jahan Ali"></option>
											<option value="Mohammadpur"></option>
											<option value="Dhunat"></option>
											<option value="Madhukhali"></option>
											<option value="Phulbari"></option>
											<option value="Dighalia"></option>
											<option value="Khalishpur"></option>
											<option value="Saghatta"></option>
											<option value="Sariakandi"></option>
											<option value="Kalia"></option>
											<option value="Lohagara"></option>
											<option value="Sonadanga"></option>
											<option value="Khulna Sadar"></option>
											<option value="Rajbari Sadar"></option>
											<option value="Shahjadpur"></option>
											<option value="Ulipur"></option>
											<option value="Kazipur"></option>
											<option value="Terokhada"></option>
											<option value="Rupsa"></option>
											<option value="Rampal"></option>
											<option value="Fulchhari"></option>
											<option value="Kurigram Sadar"></option>
											<option value="Mongla"></option>
											<option value="Bera"></option>
											<option value="Sirajganj Sadar"></option>
											<option value="Kamarkhanda"></option>
											<option value="Boalmari"></option>
											<option value="Bhurungamari"></option>
											<option value="Fakirhat"></option>
											<option value="Belkuchi"></option>
											<option value="Alfadanga"></option>
											<option value="Nageshwari"></option>
											<option value="Bagerhat Sadar"></option>
											<option value="Chilmari"></option>
											<option value="Chauhali"></option>
											<option value="Kashiani"></option>
											<option value="Mollahat"></option>
											<option value="Islampur"></option>
											<option value="Madarganj"></option>
											<option value="Daulatpur"></option>
											<option value="Morrelganj"></option>
											<option value="Gopalganj Sadar"></option>
											<option value="Dewanganj"></option>
											<option value="Saltha"></option>
											<option value="Faridpur Sadar"></option>
											<option value="Melandaha"></option>
											<option value="Goalandaghat"></option>
											<option value="Shibalaya"></option>
											<option value="Sarishabari"></option>
											<option value="Sarankhola"></option>
											<option value="Char Rajibpur"></option>
											<option value="Nagarpur"></option>
											<option value="Raumari"></option>
											<option value="Tangail Sadar"></option>
											<option value="Muksudpur"></option>
											<option value="Bhuapur"></option>
											<option value="Gopalpur"></option>
											<option value="Bakshiganj"></option>
											<option value="Kalihati"></option>
											<option value="Kachua"></option>
											<option value="Tungi Para"></option>
											<option value="Ghior"></option>
											<option value="Nagarkanda"></option>
											<option value="Chitalmari"></option>
											<option value="Harirampur"></option>
											<option value="Jamalpur Sadar"></option>
											<option value="Dhanbari"></option>
											<option value="Delduar"></option>
											<option value="Zianagar"></option>
											<option value="Mathbaria"></option>
											<option value="Patharghata"></option>
											<option value="Sreebardi"></option>
											<option value="Nazirpur"></option>
											<option value="Sherpur Sadar"></option>
											<option value="Ghatail"></option>
											<option value="Sadarpur"></option>
											<option value="Char Bhadrasan"></option>
											<option value="Bhanga"></option>
											<option value="Bhandaria"></option>
											<option value="Pirojpur Sadar"></option>
											<option value="Saturia"></option>
											<option value="Madhupur"></option>
											<option value="Manikganj Sadar"></option>
											<option value="Rajoir"></option>
											<option value="Mirzapur"></option>
											<option value="Kotali Para"></option>
											<option value="Basail"></option>
											<option value="Dhamrai"></option>
											<option value="Jhenaigati"></option>
											<option value="Barguna Sadar"></option>
											<option value="Kawkhali"></option>
											<option value="Bamna"></option>
											<option value="Kanthalia"></option>
											<option value="Nesarabad (Swarupkati)"></option>
											<option value="Amtali"></option>
											<option value="Nawabganj"></option>
											<option value="Banari Para"></option>
											<option value="Dohar"></option>
											<option value="Wazirpur"></option>
											<option value="Singair"></option>
											<option value="Rajapur"></option>
											<option value="Agailjhara"></option>
											<option value="Madaripur Sadar"></option>
											<option value="Shib Char"></option>
											<option value="Sakhipur"></option>
											<option value="Jhalokati Sadar"></option>
											<option value="Kala Para"></option>
											<option value="Nalitabari"></option>
											<option value="Betagi"></option>
											<option value="Nakla"></option>
											<option value="Muktagachha"></option>
											<option value="Gaurnadi"></option>
											<option value="Kalkini"></option>
											<option value="Kaliakair"></option>
											<option value="Mirzaganj"></option>
											<option value="Sreenagar"></option>
											<option value="Savar"></option>
											<option value="Fulbaria"></option>
											<option value="Nalchity"></option>
											<option value="Bakerganj"></option>
											<option value="Mymensingh Sadar"></option>
											<option value="Patuakhali Sadar"></option>
											<option value="Keraniganj"></option>
											<option value="Bhaluka"></option>
											<option value="Lohajang"></option>
											<option value="Zanjira"></option>
											<option value="Phulpur"></option>
											<option value="Haluaghat"></option>
											<option value="Gazipur Sadar"></option>
											<option value="Shariatpur Sadar"></option>
											<option value="Babuganj"></option>
											<option value="Serajdikhan"></option>
											<option value="Barisal Sadar (Kotwali)"></option>
											<option value="Muladi"></option>
											<option value="Sreepur"></option>
											<option value="Dumki"></option>
											<option value="Darus Salams"></option>
											<option value="Shah Ali"></option>
											<option value="Hazaribagh"></option>
											<option value="Mohammadpur"></option>
											<option value="Naria"></option>
											<option value="Adabor"></option>
											<option value="Trishal"></option>
											<option value="Galachipa"></option>
											<option value="Mirpur"></option>
											<option value="Pallabi"></option>
											<option value="Lalbagh"></option>
											<option value="Kamrangir Char"></option>
											<option value="Sher-e-bangla Nagar"></option>
											<option value="Turag"></option>
											<option value="Dhanmondi"></option>
											<option value="Damudya"></option>
											<option value="Kafrul"></option>
											<option value="New Market"></option>
											<option value="Gosairhat"></option>
											<option value="Kalabagan"></option>
											<option value="Uttara"></option>
											<option value="Tejgaon"></option>
											<option value="Chak Bazar"></option>
											<option value="Shahbagh"></option>
											<option value="Cantonment"></option>
											<option value="Ramna"></option>
											<option value="Tejgaon Ind. Area"></option>
											<option value="Bangshal"></option>
											<option value="Gulshan"></option>
											<option value="Biman Bandar"></option>
											<option value="Kotwali"></option>
											<option value="Dakshinkhan"></option>
											<option value="Tongibari"></option>
											<option value="Paltan"></option>
											<option value="Sutrapur"></option>
											<option value="Rampura"></option>
											<option value="Motijheel"></option>
											<option value="Uttar Khan"></option>
											<option value="Khilkhet"></option>
											<option value="Badda"></option>
											<option value="Khilgaon"></option>
											<option value="Gendaria"></option>
											<option value="Shyampur"></option>
											<option value="Jatrabari"></option>
											<option value="Mehendiganj"></option>
											<option value="Sabujbagh"></option>
											<option value="Kadamtali"></option>
											<option value="Bauphal"></option>
											<option value="Bhedarganj"></option>
											<option value="Narayanganj Sadar"></option>
											<option value="Dhobaura"></option>
											<option value="Hizla"></option>
											<option value="Gaffargaon"></option>
											<option value="Demra"></option>
											<option value="Munshiganj Sadar"></option>
											<option value="Kaliganj"></option>
											<option value="Dashmina"></option>
											<option value="Rupganj"></option>
											<option value="Kapasia"></option>
											<option value="Bandar"></option>
											<option value="Ishwarganj"></option>
											<option value="Sonargaon"></option>
											<option value="Gauripur"></option>
											<option value="Purbadhala"></option>
											<option value="Bhola Sadar"></option>
											<option value="Nandail"></option>
											<option value="Chandpur Sadar"></option>
											<option value="Gazaria"></option>
											<option value="Palash"></option>
											<option value="Matlab Uttar"></option>
											<option value="Araihazar"></option>
											<option value="Char Fasson"></option>
											<option value="Hossainpur"></option>
											<option value="Narsingdi Sadar"></option>
											<option value="Durgapur"></option>
											<option value="Manohardi"></option>
											<option value="Pakundia"></option>
											<option value="Haim Char"></option>
											<option value="Meghna"></option>
											<option value="Netrokona Sadar"></option>
											<option value="Daulatkhan"></option>
											<option value="Faridganj"></option>
											<option value="Roypur"></option>
											<option value="Burhanuddin"></option>
											<option value="Lalmohan"></option>
											<option value="Matlab Dakshin"></option>
											<option value="Daudkandi"></option>
											<option value="Kendua"></option>
											<option value="Kishoreganj Sadar"></option>
											<option value="Titas"></option>
											<option value="Banchharampur"></option>
											<option value="Lakshmipur Sadar"></option>
											<option value="Homna"></option>
											<option value="Hajiganj"></option>
											<option value="Kalmakanda"></option>
											<option value="Roypura"></option>
											<option value="Kamalnagar"></option>
											<option value="Ramganj"></option>
											<option value="Barhatta"></option>
											<option value="Kachua"></option>
											<option value="Atpara"></option>
											<option value="Tazumuddin"></option>
											<option value="Belabo"></option>
											<option value="Karimganj"></option>
											<option value="Bajitpur"></option>
											<option value="Tarail"></option>
											<option value="Kuliar Char"></option>
											<option value="Nabinagar"></option>
											<option value="Muradnagar"></option>
											<option value="Chandina"></option>
											<option value="Ramgati"></option>
											<option value="Nikli"></option>
											<option value="Shahrasti"></option>
											<option value="Madan"></option>
											<option value="Noakhali Sadar (Sudharam)"></option>
											<option value="Chatkhil"></option>
											<option value="Manpura"></option>
											<option value="Bhairab"></option>
											<option value="Brahman Para"></option>
											<option value="Itna"></option>
											<option value="Dharampasha"></option>
											<option value="Barura"></option>
											<option value="Mohanganj"></option>
											<option value="Ashuganj"></option>
											<option value="Hatiya"></option>
											<option value="Begumganj"></option>
											<option value="Sonaimuri"></option>
											<option value="Subarnachar"></option>
											<option value="Manoharganj"></option>
											<option value="Laksam"></option>
											<option value="Brahmanbaria Sadar"></option>
											<option value="Mithamain"></option>
											<option value="Sarail"></option>
											<option value="Austagram"></option>
											<option value="Khaliajuri"></option>
											<option value="Nasirnagar"></option>
											<option value="Tahirpur"></option>
											<option value="Comilla Adarsha Sadar"></option>
											<option value="Kasba"></option>
											<option value="Comilla Sadar Dakshin"></option>
											<option value="Senbagh"></option>
											<option value="Nangalkot"></option>
											<option value="Akhaura"></option>
											<option value="Kabirhat"></option>
											<option value="Jamalganj"></option>
											<option value="Companiganj"></option>
											<option value="Bijoynagar"></option>
											<option value="Ajmiriganj"></option>
											<option value="Chauddagram"></option>
											<option value="Lakhai"></option>
											<option value="Baniachong"></option>
											<option value="Bishwambarpur"></option>
											<option value="Derai"></option>
											<option value="Sulla"></option>
											<option value="Daganbhuiyan"></option>
											<option value="Madhabpur"></option>
											<option value="Sunamganj Sadar"></option>
											<option value="Sonagazi"></option>
											<option value="Feni Sadar"></option>
											<option value="Dakshin Sunamganj"></option>
											<option value="Habiganj Sadar"></option>
											<option value="Sandwip"></option>
											<option value="Fulgazi"></option>
											<option value="Parshuram"></option>
											<option value="Nabiganj"></option>
											<option value="Chunarughat"></option>
											<option value="Chhagalnaiya"></option>
											<option value="Mirsharai"></option>
											<option value="Jagannathpur"></option>
											<option value="Dowarabazar"></option>
											<option value="Bahubal"></option>
											<option value="Chhatak"></option>
											<option value="Sitakunda"></option>
											<option value="Sreemangal"></option>
											<option value="Maulvi Bazar Sadar"></option>
											<option value="Fatikchhari"></option>
											<option value="Bishwanath"></option>
											<option value="Balaganj"></option>
											<option value="Matiranga"></option>
											<option value="Companiganj"></option>
											<option value="Hathazari"></option>
											<option value="Pahartali"></option>
											<option value="Halishahar"></option>
											<option value="Chittagong Port"></option>
											<option value="Patiya"></option>
											<option value="Manikchhari"></option>
											<option value="Khulshi"></option>
											<option value="Sylhet Sadar"></option>
											<option value="Kamalganj"></option>
											<option value="Double Mooring"></option>
											<option value="Dakshin Surma"></option>
											<option value="Rajnagar"></option>
											<option value="Bayejid Bostami"></option>
											<option value="Panchlaish"></option>
											<option value="Kotwali"></option>
											<option value="Anowara"></option>
											<option value="Bakalia"></option>
											<option value="Chandgaon"></option>
											<option value="Raozan"></option>
											<option value="Kutubdia"></option>
											<option value="Gowainghat"></option>
											<option value="Maheshkhali"></option>
											<option value="Banshkhali"></option>
											<option value="Boalkhali"></option>
											<option value="Panchhari"></option>
											<option value="Pekua"></option>
											<option value="Lakshmichhari"></option>
											<option value="Khagrachhari Sadar"></option>
											<option value="Fenchuganj"></option>
											<option value="Kulaura"></option>
											<option value="Chakaria"></option>
											<option value="Golabganj"></option>
											<option value="Satkania"></option>
											<option value="Cox'S Bazar Sadar"></option>
											<option value="Chandanaish"></option>
											<option value="Kawkhali (Betbunia)"></option>
											<option value="Rangunia"></option>
											<option value="Mahalchhari"></option>
											<option value="Ramu"></option>
											<option value="Jaintiapur"></option>
											<option value="Naniarchar"></option>
											<option value="Dighinala"></option>
											<option value="Lohagara"></option>
											<option value="Ukhia"></option>
											<option value="Lama"></option>
											<option value="Kanaighat"></option>
											<option value="Juri"></option>
											<option value="Beani Bazar"></option>
											<option value="Rangamati Sadar"></option>
											<option value="Barlekha"></option>
											<option value="Langadu"></option>
											<option value="Teknaf"></option>
											<option value="Rajasthali"></option>
											<option value="Baghai Chhari"></option>
											<option value="Bandarban Sadar"></option>
											<option value="Naikhongchhari"></option>
											<option value="Zakiganj"></option>
											<option value="Alikadam"></option>
											<option value="Barkal"></option>
											<option value="Rowangchhari"></option>
											<option value="Belai Chhari"></option>
											<option value="Thanchi"></option>
                                        <option value="Ruma"></option>
                                    </datalist>
                                    </div>

                                </div>
                            </div>
                            <div class="reg-info">
                                <button>Submit</button>
                            </div>
                        </div>
                        <div class="col-lg-6"></div>
                        <div class="col-lg-6"></div>
                        <div class="col-lg-6"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>


<?php include('footer.php'); ?>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#test').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#tstpic").change(function() {
            readURL(this);
        }

    );

</script>
<script>
    $(document).ready(function() {
        $("#show").click(function() {
            $(".reg-number").show();
        });
        $("#hide").click(function() {
            $(".reg-number").hide();
        });

    });

</script>
