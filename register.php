<?php include('header.php'); ?>


<section id="register">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <a href="doner-register.php" class="reg-btn text-center">
                       <span class="reg-content">
                            <i class="icofont icofont-hand-drag1"></i>
                            <h4>Donor Registration</h4>
                       </span>                    
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="collect-register.php" class="reg-btn text-center">
                       <span class="reg-content">
                           <i class="icofont icofont-unity-hand"></i>
                            <h4>Collector NGO Registration</h4>
                       </span>                    
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="volentier-register.php" class="reg-btn text-center">
                       <span class="reg-content">
                           <i class="icofont icofont-help-robot"></i>
                            <h4>Volentier Registration</h4>
                       </span>                    
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include('footer.php'); ?>
