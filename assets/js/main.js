(function ($) {
    "use strict";

    jQuery(document).ready(function () {


        /*  - Scroll-top 
      	---------------------------------------------*/
        jQuery(window).on('scroll', function () {
            var scrollTop = jQuery(this).scrollTop();
            if (scrollTop > 400) {
                jQuery('.top').fadeIn();
            } else {
                jQuery('.top').fadeOut();
            }
        });

        jQuery('.top').on('click', function () {
            $('html, body').animate({
                scrollTop: 0
            }, 1000);

            return false;
        });


        /*  HOME Carousel
        ----------------------------------*/
        $(".slide-carouse").owlCarousel({
            items: 1,
            dots: true,
            autoplay: true,
            nav: false,
            loop: true,

        });
        if ($.fn.on) {
            $(".slide-carouse").on("translate.owl.carousel", function () {
                $(".slide-txt h1").removeClass("animated fadeInUp").css("opacity", "0")
            });
            $(".slide-carouse").on("translated.owl.carousel", function () {
                $(".slide-txt h1").addClass("animated fadeInUp").css("opacity", "1")
            });
        }

        /*  Partner Logo Carousel
        ----------------------------------*/
        $(".slide-partner-logo").owlCarousel({
            items: 5,
            dots: true,
            autoplay: true,
            nav: false,
            loop: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 2,
                    nav: false
                },
                800: {
                    items: 3,
                    nav: false
                },

                1170: {
                    items: 5,
                }

            }

        });

        $(".listing-carousel").owlCarousel({
            items: 4,
            loop: true,
            dots: true,
            nav: true,
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            margin: 30,
            autoplay: true,
            responsive: {
                0: {
                    items: 2,
                    nav: false
                },
                600: {
                    items: 2,
                    nav: false
                },
                800: {
                    items: 3,
                    nav: false
                },

                1170: {
                    items: 4,
                }

            }
        });



        /*  Magnifing Pop Up
        ----------------------------------*/
        if ($.fn.magnificPopup) {
            $('.gallery-content').magnificPopup({
                delegate: 'a',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0, 1]
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function (item) {

                    }
                }
            });
        }



    });


    jQuery(window).on("load", function () {
        /*  - Pre Loader
        ---------------------------------------------*/
        $(".pre-loader-area").fadeOut();

    });


}(jQuery));
