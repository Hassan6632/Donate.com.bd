<?php 

include ('header.php');
include ('config.php');

$sql = $db->query('SELECT * FROM donar_goods');
$books = $sql->fetchAll(PDO::FETCH_ASSOC);

?>

<!--    [ Strat Section Area]-->
<section id="volunteer">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Cpanel</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <?php foreach ($books as $book): ?>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="vol-pic">
                                <img src="assets/img/03.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="vol-info">
                                <h4>Preneur Lab</h4>
                                <h4>PickUp Dae :
                                    <?php echo $book['date']; ?>
                                </h4>
                                <h4>PickUp Time:
                                    <?php echo $book['time']; ?>
                                </h4>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="vol-info">
                                <h4>Address :
                                    <?php echo $book['address']; ?>
                                </h4>
                                <h4>Collector Contact :
                                    <?php echo $book['number']; ?>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<?php include ('footer.php'); ?>
