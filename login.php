<?php include ('header.php'); ?>

<style>
    header {
        display: none;
    }

</style>

<!--    [ Strat Section Title Area]-->
<section id="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="login-content text-center">
                    <div class="log-logo">
                        <img src="assets/img/logo.png" alt="">
                    </div>
                    <div class="log-in-form">
                        <form action="" method="post">
                            <input type="text" name="name" placeholder="User ID">
                            <input type="password" name="password" placeholder="Password">
                            <button><!--<i class="icofont icofont-brand-natgeo"></i>--><i class="icofont icofont-hand-drag1"></i> Log In</button>
                        </form>
                        <a href="register.php" class="reg-btn-pag">Register</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include ('footer.php'); ?>
