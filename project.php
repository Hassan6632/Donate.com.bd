<?php include('header.php'); ?>

<!--    [ Strat Section Title Area]-->
<section id="project-content">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="project-organizer-logo">
                        <img src="assets/img/prelab.png" alt="">
                    </div>
                    <a href="#" class="donate-btn"><i class="icofont icofont-money"></i> Donate</a>
                </div>
                <div class="col-lg-9">
                    <div class="organizer-project">
                        <div class="organi-title text-center">
                            <h3>CAMPAIGN SINGLE</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                    <div class="irs-campain-content">
                        <h4><a href="campaign-single.html">Give Them More Food</a></h4>
                        <div class="irs-amount clearfix">
                            <div class="skillbar clearfix " data-percent="70%">
                                <div class="skillbar-bar" style="width: 70%;"></div>
                                <div class="skill-bar-percent">70%</div>
                            </div>
                            <p class="pull-left"> Target : $78,950</p>
                            <p class="pull-right"> Collected : $55,265</p>
                        </div>
                    </div>

                    <div class="project-time">
                        <h4>Project Runing Time</h4>
                        <p id="demo"></p>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="project-detail">
                        <div class="pro-detail-content">
                            <h3>SEE OUR ACTIVITIES</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam eveniet, nesciunt, quam blanditiis reiciendis commodi asperiores sunt nobis quibusdam quas, nam natus. Voluptate nemo laudantium, sequi. Maxime nihil odio, nisi.</p>
                        </div>
                        <div class="pro-detail-piv">
                            <iframe src="https://www.youtube.com/embed/12cNBNKIXv8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>

                        <div class="pro-extra">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="listing-carousel owl-carousel">
                                        <div class="single-listing-box listing-bg-1">
                                            <div class="listing-hover">
                                                <h3>Eat &amp; Drink</h3>
                                                <p>130 Listing</p>
                                                <a href="" class="bordered-btn">Browser</a>
                                            </div>
                                        </div>
                                        <div class="single-listing-box listing-bg-2">
                                            <div class="listing-hover">
                                                <h3>Eat &amp; Drink</h3>
                                                <p>130 Listing</p>
                                                <a href="" class="bordered-btn">Browser</a>
                                            </div>
                                        </div>
                                        <div class="single-listing-box listing-bg-3">
                                            <div class="listing-hover">
                                                <h3>Eat &amp; Drink</h3>
                                                <p>130 Listing</p>
                                                <a href="" class="bordered-btn">Browser</a>
                                            </div>
                                        </div>
                                        <div class="single-listing-box listing-bg-4">
                                            <div class="listing-hover">
                                                <h3>Eat &amp; Drink</h3>
                                                <p>130 Listing</p>
                                                <a href="" class="bordered-btn">Browser</a>
                                            </div>
                                        </div>
                                        <div class="single-listing-box listing-bg-5">
                                            <div class="listing-hover">
                                                <h3>Eat &amp; Drink</h3>
                                                <p>130 Listing</p>
                                                <a href="" class="bordered-btn">Browser</a>
                                            </div>
                                        </div>
                                        <div class="single-listing-box listing-bg-6">
                                            <div class="listing-hover">
                                                <h3>Eat &amp; Drink</h3>
                                                <p>130 Listing</p>
                                                <a href="" class="bordered-btn">Browser</a>
                                            </div>
                                        </div>
                                        <div class="single-listing-box listing-bg-7">
                                            <div class="listing-hover">
                                                <h3>Eat &amp; Drink</h3>
                                                <p>130 Listing</p>
                                                <a href="" class="bordered-btn">Browser</a>
                                            </div>
                                        </div>
                                        <div class="single-listing-box listing-bg-8">
                                            <div class="listing-hover">
                                                <h3>Eat &amp; Drink</h3>
                                                <p>130 Listing</p>
                                                <a href="" class="bordered-btn">Browser</a>
                                            </div>
                                        </div>
                                        <div class="single-listing-box listing-bg-9">
                                            <div class="listing-hover">
                                                <h3>Eat &amp; Drink</h3>
                                                <p>130 Listing</p>
                                                <a href="" class="bordered-btn">Browser</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->



<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->

<script>
    // Set the date we're counting down to
    var countDownDate = new Date("Sep 5, 2018 15:37:25").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        document.getElementById("demo").innerHTML = days + " Day " + hours + " : " +
            minutes + " : " + seconds + "  ";

        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);

</script>

<?php include('footer.php'); ?>
