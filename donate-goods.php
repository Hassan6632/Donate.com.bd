<?php
include ('header.php');

include ('config.php');

if(isset($_POST['submit'])){
    $product=$_POST['product'];
    $organization=$_POST['organization'];
    $date=$_POST['date'];
    $time=$_POST['time'];
    $address=$_POST['address'];
    $number=$_POST['number'];
    $area=$_POST['area'];
    
    $stmt=$db->prepare('INSERT INTO donar_goods(product, organization, date, time, address, number, area) VALUES(:product, :organization, :date, :time, :address, :number, :area)');
    
    $stmt->bindParam(':product', $product);
    $stmt->bindParam(':organization', $organization);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':time', $time);
    $stmt->bindParam(':address', $address);
    $stmt->bindParam(':number', $number);
    $stmt->bindParam(':area', $area);
    
    $stmt->execute();
    
    if($stmt->rowCount()){
        echo 'Sucess';
    }
}


?>

    <!--    [ Strat Section Title Area]-->
    <section id="donate-goods" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Donate Goods</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="dgoods-content">
                        <form action="" method="post">
                            <div class="reg-info donate-ifo-input">
                                <span>Donate Product</span>
                                <select name="product">
                                  <option value="volvo">Clooth</option>
                                  <option value="saab">Food</option>
                                  <option value="opel">Books</option>
                                  <option value="audi">Other</option>
                                </select>
                            </div>
                            <div class="donate-ifo-input">
                                <span>prefer organization</span>
                                <select name="organization">
                                  <option value="volvo">PreneurLAB</option>
                                  <option value="saab">Begum</option>
                                  <option value="opel">EMK</option>
                                  <option value="audi">Other</option>
                                </select>

                            </div>
                            <div class="donate-ifo-input">
                                <span>Pick-Up Date</span>
                                <input type="date" name="date" placeholder="">
                            </div>
                            <div class="donate-ifo-input">
                                <span>Pick-Up Time</span>
                                <input type="time" name="time" placeholder="prefer Time">
                            </div>
                            <div class="donate-ifo-input">
                                <span>Pick-Up Address</span>
                                <textarea name="address" placeholder="Pick-Up Address"></textarea>
                            </div>
                            <div class="donate-ifo-input">
                                <span>Contact</span>
                                <input type="number" name="number" placeholder="Contact Number">
                            </div>
                            <div class="donate-ifo-input">
                                <span>Pick-Up Area</span>
                                <select name="area">
                                  <option value="volvo">Zikatola</option>
                                  <option value="saab">Dhanmodi</option>
                                  <option value="opel">ScienceLab</option>
                                  <option value="audi">Muhammad Pur</option>
                                </select>
                            </div>
                            <div class="take-photo donate-ifo-input">
                                <button><i class="icofont icofont-camera"></i> Take Photo</button>
                                <input type="submit" value="submit" name="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->


    <!--    [ Strat Section Area]-->
    <!--    [Finish Section Area]-->

    <?php include ('footer.php');?>
