<?php 

include('header.php'); 

if(isset($_POST['submit'])){
    
    $name=$_POST['name'];
    $number=$_POST['number'];
    $amount=$_POST['amount'];
    $transaction=$_POST['transaction'];
    $area=$_POST['area'];
    
    include ('config.php');
    
    $stmt=$db->prepare('INSERT INTO tk_donate(name, number, amount, transaction, area) VALUES(:name, :number, :amount, :transaction, :area)');
    
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':number', $number);
    $stmt->bindParam(':amount', $amount);
    $stmt->bindParam(':transaction', $transaction);
    $stmt->bindParam(':area', $area);
    
    $stmt->execute();
    
    if($stmt->rowCount()){
        echo 'sucess';
    }
}

?>

<!--    [ Strat Section Title Area]-->
<section id="tk-donate">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Money Doner</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="money-donate">
                        <form action="" method="post">
                            <div class="reg-info donate-ifo-input">
                                <span>Donar Name</span>
                                <input type="text" name="name" placeholder="Donar Name">
                            </div>
                            <div class="reg-info donate-ifo-input">
                                <span>Donar Phone</span>
                                <input type="nimber" name="number" placeholder="Donar Phone">
                            </div>
                            <div class="reg-info donate-ifo-input">
                                <span>Donate Amount</span>
                                <input type="number" name="amount" placeholder="Donar Phone">
                            </div>
                            <div class="reg-info donate-ifo-input">
                                <span>bkash Number / Transaction ID</span>
                                <input type="text" name="transaction" placeholder="Number/Transaction ID">
                            </div>
                            <div class="reg-info donate-ifo-input">
                                <span>Donar Area</span>
                                <input type="text" name="area" placeholder="Donar Area">
                            </div>

                            <div class="take-photo donate-ifo-input">
                                <input type="submit" value="submit" name="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->

<?php include('footer.php'); ?>
